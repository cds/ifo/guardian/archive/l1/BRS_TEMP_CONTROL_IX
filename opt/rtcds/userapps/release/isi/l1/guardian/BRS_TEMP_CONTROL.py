from guardian import GuardState
import time
import cdsutils
import math

#nominal = 'HPI_SERVO_ON_CTIDAL'

class INIT(GuardState):
    request = True

class SERVO_ON(GuardState):
    index = 10
    request = True

    def main(self):
	self.period = 3600
	self.timer['wait'] = self.period      # initialize timer for the run loop

    def run(self):
	if self.timer['wait']:
	    # average last 10 seconds of temperature
	    # TODO: include a check on standard deviation in case of a glitch ?
	    temp = cdsutils.avg(-self.period,'ISI-GND_BRS_' + BRS + '_TEMPL',stddev=False)
 	    time.sleep(0.1)
            print '##########################################'
 	    print 'temperature in the BRS box was ' + str(temp/100) + ' on avg for the last ' + str(self.period) + 'secs'
            print 'setpoint temperature is ' + str(SETPOINT)
	    # Calculate difference with setpoint
	    diff = temp/100.0-SETPOINT #self.setpoint
            print str(diff) + ' degC away from setpoint'

            if (diff-OFFSET) >= 0: 
                volt_in = math.sqrt((diff-OFFSET)/GAIN)
                print 'setting voltage to ' + str(volt_in) + ' V #notreallyhappening'
#               ezca['ISI-GND_BRS_' + BRS + '_HEATCTRLIN'] = volt_in
                # Save the time to a file for offloading
                pathname = '/opt/rtcds/userapps/trunk/isi/l1/scripts/'
                filename = BRS.lower() + '_brs_drive.txt'
                with open (pathname + filename, "a") as f:
                    f.write(str(volt_in) + ' ' + str(diff) + '\n')
            self.timer['wait']= self.period # should be the same 
	return True

class HOLD_SERVO(GuardState):
    index = 20
    request = True

    def main(self):
	return
    def run(self):
        return True
##################################################################

edges = [
    ('INIT','SERVO_ON'),
    ('SERVO_ON', 'HOLD_SERVO'),
]


